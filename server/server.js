import bodyParser from "body-parser";
import express from "express";
import path from "path";
import moment from "moment";
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const router = express.Router();

const staticFiles = express.static(path.join(__dirname, "../../client/build"));

app.use(staticFiles);

router.get("/rtp", (req, res) => {
  const data = [
    {
      interval: "29-JAN-2018 17:15",
      interval_datetime: "2018-01-29T17:15:00",
      five_min_period: 6,
      isDayLightSavingHR: 0,
      pnode: "ABY0111",
      load: 2.956,
      generation: 0.0,
      price: 60.9
    },
    {
      interval: "30-JAN-2018 17:20",
      interval_datetime: "2018-01-30T17:20:00",
      five_min_period: 6,
      isDayLightSavingHR: 0,
      pnode: "ALB0331",
      load: 119.055,
      generation: 0.0,
      price: 68.88
    },
    {
      interval: "30-JAN-2018 17:25",
      interval_datetime: "2018-01-30T17:25:00",
      five_min_period: 6,
      isDayLightSavingHR: 0,
      pnode: "ALB1101",
      load: 50.314,
      generation: 0.0,
      price: 62.61
    },
    {
      interval: "30-JAN-2018 17:30",
      interval_datetime: "2018-01-30T17:30:00",
      five_min_period: 6,
      isDayLightSavingHR: 0,
      pnode: "ALB1101",
      load: 50.314,
      generation: 0.0,
      price: 65.61
    },
    {
      interval: "30-JAN-2018 17:35",
      interval_datetime: "2018-01-30T17:35:00",
      five_min_period: 6,
      isDayLightSavingHR: 0,
      pnode: "ALB1101",
      load: 50.314,
      generation: 0.0,
      price: 63.61
    },
    {
      interval: "30-JAN-2018 17:40",
      interval_datetime: "2018-01-30T17:40:00",
      five_min_period: 6,
      isDayLightSavingHR: 0,
      pnode: "ALB1101",
      load: 50.314,
      generation: 0.0,
      price: 62.61
    },
    {
      interval: "31-JAN-2018 17:40",
      interval_datetime: "2018-01-31T17:35:00",
      five_min_period: 6,
      isDayLightSavingHR: 0,
      pnode: "ALB1101",
      load: 50.314,
      generation: 0.0,
      price: 62.61
    },
    {
      interval: "31-JAN-2018 17:40",
      interval_datetime: "2018-01-31T17:40:00",
      five_min_period: 6,
      isDayLightSavingHR: 0,
      pnode: "ALB1101",
      load: 50.314,
      generation: 0.0,
      price: 62.61
    },
    {
      interval: "31-JAN-2018 17:40",
      interval_datetime: "2018-01-31T17:45:00",
      five_min_period: 6,
      isDayLightSavingHR: 0,
      pnode: "ALB1101",
      load: 50.314,
      generation: 0.0,
      price: 62.61
    }
  ];

  let gt = moment(req.query.$filter.slice(30, 46));
  let lt = moment(req.query.$filter.slice(82, 98));

  let filterData = data.filter(x => {
    if (
      moment(x.interval_datetime).isAfter(gt) &&
      moment(x.interval_datetime).isBefore(lt)
    ) {
      return x;
    }
  });
  res.status(200).json(filterData);
});

router.post("/rtp", (req, res) => {
  const data = [
    {
      interval: "23-JUL-2015 17:40",
      interval_datetime: "2015-07-23T17:40:00",
      five_min_period: 6,
      isDayLightSavingHR: 0,
      pnode: "ALB1101",
      load: 50.314,
      generation: 0.0,
      price: 62.61
    },
    {
      interval: "23-JUL-2015 17:35",
      interval_datetime: "2015-07-23T17:35:00",
      five_min_period: 6,
      isDayLightSavingHR: 0,
      pnode: "ALB1101",
      load: 50.314,
      generation: 0.0,
      price: 63.61
    }
  ];
  res.status(200).json(data);
});

router.post("/signup", (req, res) => {
  res.send({
    body: "You will be notified when threshold passes: " + req.body.threshold
  });
});

app.use(router);

app.use("/*", staticFiles);

app.set("port", process.env.PORT || 3001);
app.listen(app.get("port"), () => {
  console.log(`Listening on ${app.get("port")}`);
});
