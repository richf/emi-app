import React, { Component } from "react";

class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: null,
      threshold: null,
      submitted: false
    };
  }

  handleInputChange = e => {
   this.setState({
     [e.target.name]: e.target.value
   });
  }

  handleSumbit = async (e) => {
    const data = { email: this.state.email, threshold: this.state.threshold }

    const response = await fetch("/signup", {
      method: 'POST',
      body: JSON.stringify(data),
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    });
    const submitted = await response.json();

    this.setState({ submitted: submitted.body });
  };


  render() {
    let success = null;
    if(this.state.submitted){
      success = <div className="alert alert-success">{this.state.submitted ? this.state.submitted : '' }</div>;
    }

    return (
      <div className="Signup">
        <div className="form-group">
          <label>
            Get Notified when price hits threshold
          </label>
          <input
            name="email"
            type="email"
            className="form-control"
            id="inputEmail"
            aria-describedby="email"
            placeholder="Enter email"
            onChange={this.handleInputChange}
          />
        </div>
        <div className="form-group">
          <input
            name="threshold"
            type="text"
            className="form-control"
            id="inputThreshold"
            aria-describedby="emailHelp"
            placeholder="Enter Threshold"
            onChange={this.handleInputChange}
          />
        </div>
        <div className="form-group">
          <button
            className="form-control btn btn-outline-dark"
            onClick={this.handleSumbit}
          >
            Submit
          </button>
        </div>
        {success}
      </div>
    );
  }
}

export default Signup;
