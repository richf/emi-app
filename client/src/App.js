import React, { Component } from "react";
import moment from "moment";
import DatePicker from "react-datepicker";
import { Line } from "react-chartjs-2";
import Signup from "./signup";

import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import "react-datepicker/dist/react-datepicker.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dateTimeStart: moment().subtract(1, "week"),
      dateTimeEnd: moment(),
      activeButton: null,
      rtp: [
        {
          interval_datetime: 0,
          price: 0
        }
      ]
    };
  }

  componentDidMount() {
    this.requestData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.dateTimeStart !== this.state.dateTimeStart ||
      prevState.dateTimeEnd !== this.state.dateTimeEnd
    ) {
      if(this.state.activeButton !== "hour"){
        this.requestData();
      }
    }
  }

  requestData = async () => {
    const filter =
      "$filter=interval_datetime gt datetime'" +
      moment(this.state.dateTimeStart).format(moment.HTML5_FMT.DATETIME_LOCAL) +
      "' and interval_datetime lt datetime'" +
      moment(this.state.dateTimeEnd).format(moment.HTML5_FMT.DATETIME_LOCAL) +
      "'";
    const response = await fetch("/rtp/?" + filter);
    const rtp = await response.json();

    this.setState({ rtp: rtp });
  };

  postData = async () => {
    const filter =
      "$filter=interval_datetime gt datetime'" +
      moment(this.state.dateTimeStart).format(moment.HTML5_FMT.DATETIME_LOCAL) +
      "' and interval_datetime lt datetime'" +
      moment(this.state.dateTimeEnd).format(moment.HTML5_FMT.DATETIME_LOCAL) +
      "'";
    const response = await fetch("/rtp/?" + filter, {
      method: "POST"
    });
    const rtp = await response.json();

    this.setState({ rtp: rtp });
  };

  handleClickDateRange = e => {
    let dateTimeStart = moment();

    if (e.target.value === "hour") {
      dateTimeStart = moment().subtract(1, "hour");
    } else if (e.target.value === "day") {
      dateTimeStart = moment().subtract(1, "day");
    } else if (e.target.value === "week") {
      dateTimeStart = moment().subtract(1, "week");
    }

    this.setState({
      dateTimeStart: dateTimeStart,
      dateTimeEnd: moment(),
      activeButton: e.target.value
    });

    if (e.target.value === "hour") {
      this.postData();
    }
  };

  handleStartDateChange = startDateTime => {
    this.setState({
      dateTimeStart: startDateTime,
      activeButton: null
    });
  };

  handleEndDateChange = endDateTime => {
    this.setState({
      dateTimeEnd: endDateTime,
      activeButton: null
    });
  };

  render() {
    console.log(this.state);

    const data = {
      labels: this.state.rtp.map(fiveMin =>
        moment(fiveMin.interval_datetime).format("DD MMM - h:mm a")
      ),
      datasets: [
        {
          label: "EMI - Chart",
          fill: false,
          lineTension: 0,
          backgroundColor: "#36D7B7",
          borderColor: "#36D7B7",
          pointBorderColor: "rgba(75,192,192,1)",
          data: this.state.rtp.map(fiveMin => fiveMin.price)
        }
      ]
    };

    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">EMI Price Chart</h1>
        </header>
        <div className="container">
          <div className="row">
            <div className="col-sm-8">
              <Line data={data} />
            </div>
            <div className="col-sm-4">
              <div className="form-group">
                <label>Date From:</label>
                <DatePicker
                  selected={this.state.dateTimeStart}
                  onChange={this.handleStartDateChange}
                  maxDate={this.state.dateTimeEnd}
                  showTimeSelect
                  dateFormat="DD/MM/YYYY HH:mm"
                />
              </div>
              <div className="form-group">
                <label htmlFor="dateEnd">Date To:</label>
                <DatePicker
                  selected={this.state.dateTimeEnd}
                  maxDate={moment()}
                  onChange={this.handleEndDateChange}
                  showTimeSelect
                  dateFormat="DD/MM/YYYY HH:mm"
                />
              </div>
              <div className="form-group">
                <button
                  className={`${
                    this.state.activeButton === "hour" ? "active" : ""
                  } form-control btn btn-outline-dark`}
                  value={"hour"}
                  onClick={this.handleClickDateRange}
                >
                  Last Hour
                </button>
              </div>
              <div className="form-group">
                <button
                  className={`${
                    this.state.activeButton === "day" ? "active" : ""
                  } form-control btn btn-outline-dark`}
                  value={"day"}
                  onClick={this.handleClickDateRange}
                >
                  Last Day
                </button>
              </div>
              <div className="form-group">
                <button
                  className={`${
                    this.state.activeButton === "week" ? "active" : ""
                  } form-control btn btn-outline-dark`}
                  value={"week"}
                  onClick={this.handleClickDateRange}
                >
                  Last Week
                </button>
              </div>
              <Signup />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
